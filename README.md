# Return On Investment

## Features
- Calculate the monthly income
- Calculate the yearly income
- Calculate the total Return of Investment/ Cap Rate of a property

---
## Contributors
* Detravious Brinkley <d.brinkley97@gmail.com>

---
## Run Code
1.	Clone Repo
2.  Open xcode

---
## License & copyright
- Copyright 2018-2020 FitToCode & Bros Real Estate. Released under the [FTC](https://fittocode.com) license.

